class Validator {
  static final Validator _singleton = Validator._internal();

  factory Validator() {
    return _singleton;
  }

  final requiredText = 'Requerido';
  final invalidFormatText = 'Formato Invalido';
  final incorectValidate = 'Validación incorrecta';

  Validator._internal();

  String? requiredValue(String? v, {String? text}) =>
      (v ?? '').isEmpty ? text ?? requiredText : null;

  String? emailFormat(String? v, {String? text}) {
    const emailRegex = '[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}';
    final hasMatch = RegExp(emailRegex).hasMatch(v ?? '');
    if (hasMatch) {
      return null;
    } else {
      return text ?? invalidFormatText;
    }
  }

  String? length(
    String? v,
    int size, {
    String? text,
  }) {
    if (v?.length == size) {
      return null;
    } else {
      return text ?? invalidFormatText;
    }
  }

  String? onlyNumbers(
    String? v, {
    String? text,
  }) {
    const emailRegex = '[0-9]*}';
    final hasMatch = RegExp(emailRegex).hasMatch(v ?? '');
    if (hasMatch) {
      return null;
    } else {
      return text ?? invalidFormatText;
    }
  }

  String? doubleFormat(String? v, {String? text}) =>
      double.tryParse(v ?? '') != null ? null : text ?? invalidFormatText;

  String? equals(String? value1, String? value2, {String? text}) =>
      value1 == value2 ? null : text ?? incorectValidate;

  String? latLong(String? v, {String? text}) {
    const regexp =
        r'^[-+]?([1-8]?\d(\.\d+)?|90(\.0+)?),\s*[-+]?(180(\.0+)?|((1[0-7]\d)|([1-9]?\d))(\.\d+)?)$';
    final hasMatch = RegExp(regexp).hasMatch(v ?? '');
    if (hasMatch) {
      return null;
    } else {
      return text ?? invalidFormatText;
    }
  }
}
